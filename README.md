# Calculator

1. 以下のコマンドでダウンロードできます
  ``` sh
  git clone https://bitbucket.org/DabinLapau/calculator_python.git
  ```

2. VSCode で Git Historyをダウンロードします。これ
![enter image description here](./img/githistory.png) 

3. Cmd+Shift+p 押してからの view history ってうつとおそらく以下のような感じの出てくるのでEnterで選択
![](./img/viewhistory.png)

4. 以下のような画面に飛ぶはず。下から順に新しいバージョンになってるから順に確認してみてください！(ignore imgui.iniとadd flake8 and black はあんま関係ない)
  ![](./img/hisotry.png)
  例えば add mock code　をクリックすると
  ![](./img/historyclicked.png)
  みたいになるので、main.py からの view file contents クリックでその時点でのファイルの内容を確認できる