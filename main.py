import glfw
import OpenGL.GL as gl
import imgui
import numpy as np
from imgui.integrations.glfw import GlfwRenderer


class Calculator:
    def __init__(self):
        self.register = 0
        self.cache = 0
        self.subcache = 0
        self.fReset = False

    def run(self):
        imgui.begin("Cal", True)
        imgui.set_window_size(200, 350)

        self.display()
        self.num_buttons()
        self.arithmetic_buttons()

        imgui.end()

    def display(self):
        imgui.text(f"{self.register}")

    def num_buttons(self):
        aClicked = []

        # ui render
        # -----------------------------------------------
        for i in range(3):
            aClicked.append(imgui.button(f"{i * 3 + 1}", 50, 50))
            imgui.same_line()
            aClicked.append(imgui.button(f"{i * 3 + 2}", 50, 50))
            imgui.same_line()
            aClicked.append(imgui.button(f"{i * 3 + 3}", 50, 50))
        aClicked.insert(0, imgui.button("0", 50, 50))

        # calculation
        # -----------------------------------------------
        for i, pressed in enumerate(aClicked):
            if pressed:
                if self.fReset:
                    self.register = 0
                    self.fReset = False
                self.register = self.register * 10 + i

    def arithmetic_buttons(self):
        plus = False
        multi = False
        sub = False
        div = False
        equal = False

        # ui render
        # -----------------------------------------------
        plus = imgui.button("+", 50, 50)
        imgui.same_line()
        sub = imgui.button("-", 50, 50)
        # newline
        multi = imgui.button("×", 50, 50)
        imgui.same_line()
        div = imgui.button("/", 50, 50)
        # newline
        equal = imgui.button("=", 50, 50)

        # calculation
        # -----------------------------------------------
        if plus:
            self.cache += self.register
            self.register = 0
        if multi:
            self.cache *= self.register
            self.register = 0
        if sub:
            self.cache -= self.register
            self.register = 0
        if div:
            self.cache /= self.register
            self.register = 0
        if equal:
            answer = self.cache
            answer += self.register
            self.register = answer
            self.fReset = True
            self.cache = 0


def main():
    imgui.create_context()
    window = impl_glfw_init()
    impl = GlfwRenderer(window)

    calc = Calculator()

    while not glfw.window_should_close(window):
        # Events
        # -----------------------------------------------
        glfw.poll_events()
        impl.process_inputs()
        # Render
        # -----------------------------------------------
        imgui.new_frame()
        if imgui.begin_main_menu_bar():
            if imgui.begin_menu("File", True):
                clicked_quit, selected_quit = imgui.menu_item(
                    "Quit", "Ctrl+Q", False, True
                )
                if clicked_quit:
                    exit(1)
                imgui.end_menu()
            imgui.end_main_menu_bar()
        # Window 3 Calculator Window
        # -----------------------------------------------
        calc.run()

        gl.glClear(gl.GL_COLOR_BUFFER_BIT)
        imgui.render()
        impl.render(imgui.get_draw_data())
        glfw.swap_buffers(window)
    impl.shutdown()
    glfw.terminate()


def impl_glfw_init():
    width, height = 1280, 720
    window_name = "minimal Imgui/GLFW3 example"
    if not glfw.init():
        print("couldnt init opengl context")
        exit(1)
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)
    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(int(width), int(height), window_name, None, None)
    glfw.make_context_current(window)
    if not window:
        glfw.terminate()
        print("Could not initialize Window")
        exit(1)
    return window


if __name__ == "__main__":
    main()
